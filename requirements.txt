certifi==2019.3.9
chardet==3.0.4
elevate==0.1.3
idna==2.8
requests==2.21.0
urllib3==1.24.2
